Vide MRI Viewer
=================


Starting the viewer
-------------------

The viewer can be started without initially loading images.
Starting it by command line, it is possible to load images at startup with the
following options

-i      load image
-z      load image with two color maps (for negative and positive values)

Example:

::

    $ vviewer.py -i mni.v

or

::

    $ vviewer.py -i mni.nii -z func.nii

You can open multiple viewers with the link option *-l*.
Then every image (with a *-z* option) is opened in a different viewer. Images opened with *-i* will be opened in all viewers.
Example:

::

    $ vviewer.py -i mni.nii -z zmap.nii func.nii -l

In this example one windows are opened each with the mni.nii-file and the first with the zmap and the second with the functional image.

Moving the crosshair or changing the frame in one viewer in linked mode will move the crosshair and change the frame in all others.


Basic Navigation
----------------

Also, 'up' and 'down', 'left' and 'right' and 'page up' and 'page down' can be
used to navigate through the volume.
(Instead of 'page up' and 'page down', ',' and '.' can be used.)
The directions of these keys are tied to the currently focused slice.
The scrolling wheel can also be used to zoom in or out.
Panning is possible with the right mouse button.

**Remark:** The zoom center is always the crosshair in the slice that the mouse is placed in. This might lead to the crosshair moving outside the view in the other slices. Positioning the crosshair in the center of every slice will mostly prevent this behaviour.


Widgets
=======

The viewer has some tools that are mostly self-explanatory.
Short help texts are shown if the mouse is kept over the buttons for a little
longer.


Image list
----------

Images are rendered according their order in the image list with the image at the top being rendered last potentially occluding the ones below.
The order can be changed by selecting an image and clicking the up and down buttons.
Most other widgets are updated with selecting a certain image.
Also most functions and tools work for this currently selected image.
Images can be added and removed from the viewer by clicking the plus or minus buttons.
The visibility of an image can be toggled with the checkbox in the image list.

The image list can also be used to open extra viewing windows. Rightclicking an item opens a small menu with the possible option to move it to a new window.
Selecting this will open three slices in a separate window with the selected image.
More images can be added or removed from those extra windows by rightclicking on them in the image list and selecting the appropriate menu item.
This might be helpful when comparing two images side by side.

Tool buttons
------------

Some tool buttons are located underneath the image list.

* **Crosshair toggle** Toggles the crosshair on/off.
* **Recenter image** Recenters the view and zoom to the images.
* **Find minimum** Finds the minimum in a cube of certain width (can be manually changed under *Preferences*).
* **Find maximum** Finds the maximum.
* **Toggle linked views** This toggles on/off the linking of the axis of the slices and possible extra view windows.
* **Voxel/coordinate system coordinates** Toggles between using original voxel coordinates of the images in the coordinate boxes or using coordinates with respect to the affine transformation in the image header.


Coordinate boxes and intensity values
-------------------------------------

The coordinate boxes show the coordinates of the crosshair either in original voxel coordinates of the current image or in coordinates with respect to the affine transformation.
They can be edited to move the crosshair position.

The image intensities at the crosshair and cursor position are displayed below the coordinate boxes.

**Remarks:**

  * Sometimes, after editing the position manually the coordinates will jump to different values. That is because the crosshair is moved to the center of the closest (resampled) voxel and takes its coordinates.

  * The given intensities always correspond to the currently selected image and not to the image that might be visible in the viewer. This should be kept in mind when switching off visibilities of changing the order of the images in the image list.


Threshold sliders, color maps and threshold boxes
-------------------------------------------------

The two threshold sliders can be used to set the thresholds of the color maps of the current image.
If the selected image is an underlay (loaded with *-i*), the negative slider is disabled as are the text fields for the negative color map thresholds.
For a more precise setting of the thresholds the text fields can be used.
To reset the thresholds to their default values you can use the reset buttons on the right side of the text fields and color maps.
Right-clicking the color map itself will open a number of color maps to choose from.

For every image you can switch between having one or two color maps (see *Image Settings* in the *Image* menu).

**Remark:** It might happen that you want to set the thresholds outside of the range of the intensity values. This can only be done with the thresholds boxes typing in the new thresholds because the sliders operate only within the intensity range of the (resampled) image. Using the sliders will reset the thresholds within the actual intensity range again.


Functional image frame selection
--------------------------------

The functions for frame selection are enabled as soon as a functional image is loaded.
It is possible to jump to the previous, the next, the first or the last frame.
One can also edit the frame number manually or move the slider to the desired position.
The play mode will iterate over all frames and jump from the last to the first frame thus cycling through all frames in an endless loop.
It can be turned on/off with the button or pressing the space bar.


Popout Windows
--------------

If the slices are too small in the main window of the viewer one can open individual planes in separate windows and resize them as desired.
This can be done by right-clicking on the slice and choosing *View slice in new window*.
Note that this option is only available for the main window.


The Menu
========

The menu bar offers a lot of tools that are not available in the main window.


Resampling menu
---------------

The menu item *Resampling* lets you select between three different resampling options.

The first (*Apply affine transformation*) is the standard method applied when loading images: it determines the bounding box for all images and resamples them to the resolution of the highest resolved image. The resolution can be set even higher by selecting an oversampling ratio larger than 1.

The second method uses the coordinate space and shape of the currently selected image and resamples all other images to this shape and space.
This comes in handy when you want to operate on the original voxels of an image.
Also when viewing functional images, it makes it easier to resample the image in play mode so the frame rate increases.

The third should only be used if the image headers are missing information about the affine transformations.
It scales all images along each dimension such that they match in size.
The images might look distorted.

**Remark:** Resampling is done on all images and the entire data cube and thus is a costly operation computationally. It might take a few seconds to update the images. This also holds for changing the interpolation in the *Image Settings*.


Image menu
----------

The *Image Settings* in the *Image* menu offers to set the alpha value of an image, change how it is rendered on top of other images (*overlay mode*), change its interpolation style, change the number of its color maps, and specify at which thresholds the image will go into saturation or is simply clipped.
The option *Discrete CM* can be used for images having only a few discrete intensity values.
It will assign them colors that are evenly spaced in hue.

**Remarks:**

  * The overlay mode *additive* can be used to visualize the intersection of two images: just use two monochrome color maps for the images. Now the intersection is colored in the additive color mixture, e.g. in yellow for red and green color maps.
  * The discrete color map property cannot be copied to other images in the *copy image properties* function in *Tools*!

For functional images you can open a time series plot of the voxel at the current crosshair position.
In the functional dialog you can reset the TR in case it was incorrectly loaded from the image file header and open and close design files to be visualized in the time series plot.
Additionaly, the *Time Averages* tab lets you compute the average over certain experimental conditions given a design file is provided.


Tools
-----

A very practical tool to change the thresholds is the histogram tool (shortcut 'h').
It shows a histogram of the (resampled) current image with the color map thresholds of the current image as a linear region. It changes with the selection of another image.
It is possible to move the linear regions for the color map thresholds.
If the image is a functional (4D) image then the histogram is that of the current frame. Changing the frame will update the histogram if the 'play'-function is not used.

**Remark:** Sometimes the histogram is badly scaled although the viewer tries its best to scale it well. Scaling the y-axis correctly without panning away from 0 can be done by position the mouse cursor below the y-axis labels and the x-axis and using the mouse wheel.

Sometimes, especially for z-maps, it is useful to use the same thresholds and color maps for all images. Clicking the menu item *Copy image properties* will copy the thresholds and color maps of the currently selected image to *all* other images, whether they are z-maps, functional images or other.

The menu item *Show all values* opens the so-called value window.
It shows all image intensities at the crosshair and at the cursor together with their original voxel coordinates (the coordinates in the original image array).

The *mosaic view* is a different way of viewing a 3D image.
Given a plane, a start and end voxel coordinate (resampled) and the number of rows and columns it will show slices of that plane in a separate window. The viewing modalities are copied from the main window.
The new window is not updated when changing the image properties like thresholds or color maps but has to be interpreted as a snapshot of the main window.
However, the images can be scaled and panned with their axis linked.

For ease of choosing the desired values, colored lines are added to the slices of the main window to visualize the position of the slices. These lines change with editing the start, end or rows and columns line edits or the directional plane.
After the dialog is closed the lines will disappear.
When openen/closing the mosaic dialog the crosshair will be toggled off/on to better show the slice lines. It can be manually toggled off and on again at any time.

A further text field in the mosaic dialog provides information about the distance between slices. If this distance is an integer number (emphasized by bold print) the slices will be evenly spaced over the resampled image.

**Remark:** Suppose you want to evenly slice an image along a plane of the original data cube. You can do that with resampling to the image ("Resample to current image" in the resampling menu). The resampled voxels are equivalent to the original voxels of the image then. Choosing values such that the increment is an integer will give the desired mosaic view.


Preferences
-----------

The menu item *Search Min/Max Width* lets you changed the width of the cube which is searched for extrema when using the "Find Min"/"Find Max" buttons.
A general preference can be set in "Preferences" and is used the next time the viewer is opened.

The preference menu has four subcategories with options that mostly concern the behaviour of the viewer when it is started or when new images are loaded.

  * **Viewing Options**: One can set which coordinates should be displayed, what type of linking the slice windows should have and how large the window of the viewer should be when the viewer is started.
  * **Color maps**: Here, one can choose which color maps are used by default for underlays or overlays (including functional images) and also at which thresholds the color maps clip the image or saturate.
  * **Resampling**: One can choose interpolation types and the oversampling ratio. An oversampling ratio of 1.0 should suffice but it can be chosen larger.  Note however, that this will increase the resampling time cubicly.


.. raw:: pdf

   PageBreak


Shortcuts - Overview
====================


+-----------------------+----------------------------------------+
| x                     | toggle crosshair display               |
+-----------------------+----------------------------------------+
| r                     | recenter view and reposition crosshair |
+-----------------------+----------------------------------------+
| Ctrl++                | zoom in                                |
+-----------------------+----------------------------------------+
| Ctrl+-                | zoom out                               |
+-----------------------+----------------------------------------+
| v                     | toggle visibility of current image     |
+-----------------------+----------------------------------------+
| w                     | select image above (in list)           |
+-----------------------+----------------------------------------+
| s                     | select image below (in list)           |
+-----------------------+----------------------------------------+
| d                     | deselect all but current images /      |
|                       | select all images                      |
+-----------------------+----------------------------------------+
| n                     | next frame (for functional images)     |
+-----------------------+----------------------------------------+
| b                     | previous frame                         |
+-----------------------+----------------------------------------+
| space bar             | play sequence (functional images)      |
+-----------------------+----------------------------------------+
| t                     | show time series data for current voxel|
+-----------------------+----------------------------------------+
| i                     | open image settings                    |
+-----------------------+----------------------------------------+
| f                     | open functional image settings         |
+-----------------------+----------------------------------------+
| p                     | open preference settings               |
+-----------------------+----------------------------------------+
| m                     | open the mosaic dialog                 |
+-----------------------+----------------------------------------+
| c                     | copy image properties                  |
+-----------------------+----------------------------------------+
| h                     | open the histogram tool                |
+-----------------------+----------------------------------------+
