#!/bin/sh

ubuntu_long="$(lsb_release -r)"
prefix_remove="Release: "
ubuntu_short="$(echo $ubuntu_long | sed "s/^$prefix_remove//")"

echo -n Enter release name, archive name will be vviewer_ubuntu_$ubuntu_short'_v'
read name_release

archive_pre="vviewer_ubuntu_"$ubuntu_short
name_archive=$archive_pre"_v"$name_release
echo "Creating archive with name: $name_archive"

tar -cvf $name_archive.tar *
